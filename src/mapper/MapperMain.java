package mapper;

import java.awt.EventQueue;

import gui.MapperView;

public class MapperMain {

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MapperView mv = new MapperView();
					MapperController mc = new MapperController(mv);
					mv.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		
		
	}

}
