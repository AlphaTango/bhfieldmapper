package mapper;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;

import javax.swing.JCheckBox;
import javax.swing.JFileChooser;

import org.alphatango.console.Console;
import org.alphatango.console.Console.ConsoleLevel;

import BhTokenGenerator.BhTokenGenerator;
import gui.MapperView;
import mapExporter.MapExporter;

/**
 * Class that defines the Controller for a BH Field mapper
 * @author Leigh Plumley
 * @version {@value #ver}
 */

/*
 * TODO:
 * 		- Tidy up code
 * 		- Shutdown
 * 		-- use JFrame.HIDE_ON_CLOSE instead of EXIT_ON_CLODE
 * 		-- make sure all resources are released
 */
public class MapperController implements Runnable { // doesn't need to be Runnable, run() is never called...
	
	public static final float ver = 0.98f;				// used by javadocs	
	
	private String readMe = new String(
			"BH Field Map Exporter - (c) Leigh Plumley 2017. Version: " + ver + "\n"
					+ "+ Exports Field Maps from Bullhorn to CSV." + "\n"
					+ "+ Special thanks: Mike Kakos, Mark O'Keefe, Nathan Dickerson and all my colleagues in Support\n"
					+ "+ Private Label ID is optional, specify one or more ID's using comma seperated list\n"
					+ "+ If PLID is not specified, user's primary PLID will be used\n"
					+ "+ Suggestions, CSATs, bug reports etc to:\n"
					+ "lplumley@bullhorn.com");
	
	//console
	protected Console console; // Debug output console objects
	String callerName = "Controller";

	
	private static BhTokenGenerator bhTokenGenerator;
	private static MapExporter mapper;
	private MapperView view;
	private JFileChooser fc;

	String[] creds = new String[5];

	/**
	 * Constructor
	 * @param view
	 */
	public MapperController(MapperView view){
		
		console = Console.getInstance();	//create console instance
		console.setReadMe(readMe);
		console.out(readMe, ConsoleLevel.INFO, callerName);
		
		bhTokenGenerator = new BhTokenGenerator();
		this.view = view;
		this.view.addActionListeners(new ButtonListener());
		this.view.addCloseListener(new CloseListener());
		fc = new JFileChooser();
	
	}
	
	
	private class CloseListener implements WindowListener{

		//window listener methods
		@Override
		public void windowClosing(WindowEvent e) {	// when the 'close window' button is clicked, a WindowEvent is fired in MapperView
			if (bhTokenGenerator.isConnected()) {
				bhTokenGenerator.disconnect(0);	
			}
			view.setVisible(false);
			view.dispose();
			System.exit(0);
		}
		
		@Override
		public void windowActivated(WindowEvent arg0) {}
		@Override
		public void windowClosed(WindowEvent e) {}
		@Override
		public void windowDeactivated(WindowEvent e) {}
		@Override
		public void windowDeiconified(WindowEvent e) {}
		@Override
		public void windowIconified(WindowEvent e) {}
		@Override
		public void windowOpened(WindowEvent e) {}
		
	}

	
	class ButtonListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent actionEvent) {

			if (actionEvent.getActionCommand()== "Download"){

				if (view.getTabbedPane().getSelectedIndex() == 0 /*getRdbtnUsername().isSelected()*/) { 
					// use username + password + etc
					// connect(params)
					getUsernameCreds();

					if(!bhTokenGenerator.isConnected()){


						/*if (true){*/
							//AuthMan.setCreds(creds);
							bhTokenGenerator.setCreds(creds);
						/*} else {
							console.out("*** MapperGUI *** - Invalid credentials, connection aborted");
							//downloadBtn.setEnabled(true);						// re-enable download btn TODO: add getter in MapperView class 
							return;
						}*/
					}

					startDownload();
					
				}

				else if ( view.getTabbedPane().getSelectedIndex() == 1 /*view.getRdbtnTokens().isSelected()*/) {
					// user sl+corp+rest token
					//connect(params)
					getTokenCreds();
					bhTokenGenerator.setConnected(true);
					startDownload();
					//bhTokenGenerator.setConnected(false);
				}


			}

			if (actionEvent.getActionCommand() == "Default"){
				for (JCheckBox i : view.getChkBoxes()){
					if (i.getName().equals("Candidate")
							|| i.getName().equals("ClientContact")
							|| i.getName().equals("ClientCorporation") 
							|| i.getName().equals("JobOrder")
							|| i.getName().equals("Placement")
							) {
						i.setSelected(true);
					} else {
						i.setSelected(false);
					}
				}
			}
			if (actionEvent.getActionCommand() == "+Job Tracks") {
				for (JCheckBox i : view.getChkBoxes()){
					if (i.getName().equals("JobOrder2") 
							|| i.getName().equals("JobOrder3") 
							|| i.getName().equals("JobOrder4")
							|| i.getName().equals("JobOrder5")
							) {
						i.setSelected(true);
					}
				}
			}
			if (actionEvent.getActionCommand() == "+Placement Tracks") {
				for (JCheckBox i : view.getChkBoxes()){
					if (i.getName().equals("Placement2") 
							|| i.getName().equals("Placement3") 
							|| i.getName().equals("Placement4")
							|| i.getName().equals("Placement5")
							) {
						i.setSelected(true);
					}
				}
			}
			if (actionEvent.getActionCommand() == "Enterprise+CRM") {
				for (JCheckBox i : view.getChkBoxes()){
					if (i.getName().equals("Candidate") 
							|| i.getName().equals("ClientContact")
							|| i.getName().equals("ClientCorporation") 
							|| i.getName().equals("JobOrder")
							|| i.getName().equals("Placement")
							|| i.getName().equals("Lead")
							|| i.getName().equals("Opportunity")
							) {
						i.setSelected(true);
					}
					else {
						i.setSelected(false);
					}
				}
			}
			if (actionEvent.getActionCommand() == "Select all") {
				for (JCheckBox i : view.getChkBoxes()){
					i.setSelected(true);
				}
			}
			if (actionEvent.getActionCommand() == "Deselect all") {
				for (JCheckBox i : view.getChkBoxes()){
					i.setSelected(false);
				}
			}
			
			
			// handle console level combo box
			if (actionEvent.getSource() == view.getConsoleLevelComboBox()){
				
				int level = view.getConsoleLevelComboBox().getSelectedIndex();
				
				switch (level) {
				case 0: {	// console off
					console.getScrollPane().setVisible(false);
					view.setSize(640, 298);
					console.setOutputLevel(level);
					//view.pack();
					//credsPanel.getCbShowConsole().setSelected(false);		// TODO: bit of a hacky solution, better to remove the checkbox altogether at some point
					break;
				}
				case 1: case 2:  case 3: { 
					if (console.getScrollPane().isVisible() == false) {
						console.getScrollPane().setVisible(true);
						view.setSize(640, 650);
						//credsPanel.getCbShowConsole().setSelected(true);	// TODO: as above
					}
					console.setOutputLevel(level - 1);
					break;
					
				}
				
				} // end switch
			}
		}
		

		private void startDownload() {
			fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			int returnVal = fc.showSaveDialog(view);

			if (returnVal == JFileChooser.APPROVE_OPTION) {
				File file = fc.getSelectedFile();
				//mapper = new BHCandidateMapper(this, AuthMan, file);
				mapper = new MapExporter(bhTokenGenerator, file);		// TODO: Disable user interface
				mapper.setEntities(getEntities());
				mapper.setPlid(getPlid());
				mapper.setIncludeSystem(view.getCbExcludeSystem().isSelected());
				new Thread(mapper).start();

				// TODO: change the ;download' button to 'stop' add the ability to stop the export
				
			} else {
				console.out("Save command cancelled by user.", ConsoleLevel.WARNING, callerName);
				//downloadBtn.setEnabled(true);			// disable button until download is complete 
				/*bhTokenGenerator.setRestUrl(view.getTfSl().getText(), view.getTfCorpToken().getText());
				bhTokenGenerator.setConnected(true);*/
			}
			
		}

	}
	
	private void getUsernameCreds() {
		
		creds = view.getCreds();
		bhTokenGenerator.setCreds(creds);
	}
	
	private void getTokenCreds(){
		String sl = view.getTfSl().getText();
		String corpToken = view.getTfCorpToken().getText();
		String restToken = view.getTfRestToken().getText();
		
		String url = "https://rest" + sl + ".bullhornstaffing.com/rest-services/" + corpToken + "/";

		bhTokenGenerator.setRestUrl(url);
		bhTokenGenerator.setBhToken(restToken);
	}
	
	
	//TODO: fix this, returns all entities, unticked boxes add 'null' to the array. use arraylist instead? 
	public JCheckBox[] getEntities(){
		/*String[] entities = new String[(view.getChkBoxes().length)]; //-1 ??
		System.out.println(view.getChkBoxes().length);
		int j = 0;
		for (JCheckBox i : view.getChkBoxes()) {	// use controller instead
			if (i.isSelected()) {
				entities[j] = i.getName();
			}
			j++;
		}*/
		
		return view.getChkBoxes();
		
	}
	
	public String[] getPlid(){
		
		String tmp = view.getTfPlid().getText();
		if (tmp.isEmpty()) {
			console.out("No Private Label ID(s) specified", ConsoleLevel.WARNING, "MapperController");
		}
		else {
				String[] plidArray = tmp.split(",");
				return plidArray;
		}
		return null;		// double check this is ok
		
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

}
