package pojo;

public class CandidateAPI {

    private String entityMetaUrl;

    private String entity;

    private String label;

    private CandidateFields[] fields;

    public String getEntityMetaUrl ()
    {
        return entityMetaUrl;
    }

    public void setEntityMetaUrl (String entityMetaUrl)
    {
        this.entityMetaUrl = entityMetaUrl;
    }

    public String getEntity ()
    {
        return entity;
    }

    public void setEntity (String entity)
    {
        this.entity = entity;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public CandidateFields[] getFields ()
    {
        return fields;
    }

    public void setFields (CandidateFields[] fields)
    {
        this.fields = fields;
    }

    @Override
    public String toString()
    {
        return "Entity: [entityMetaUrl = "+entityMetaUrl+", entity = "+entity+", label = "+label+", fields = "+fields+"]";
    }
    
    public String csvOuput()
    {
    	return entityMetaUrl+ "," + label + "," + fields;
    }

}
