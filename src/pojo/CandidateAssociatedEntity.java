package pojo;

public class CandidateAssociatedEntity {
    private String entityMetaUrl;

    private String entity;

    private String label;

    private CandidateAssociatedEntFields[] fields;

    public String getEntityMetaUrl ()
    {
        return entityMetaUrl;
    }

    public void setEntityMetaUrl (String entityMetaUrl)
    {
        this.entityMetaUrl = entityMetaUrl;
    }

    public String getEntity ()
    {
        return entity;
    }

    public void setEntity (String entity)
    {
        this.entity = entity;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    public CandidateAssociatedEntFields[] getFields ()
    {
        return fields;
    }

    public void setFields (CandidateAssociatedEntFields[] fields)
    {
        this.fields = fields;
    }

    @Override
    public String toString()
    {
        return "Associated entity: [entityMetaUrl = "+entityMetaUrl+", entity = "+entity+", label = "+label+", fields = "+fields+"]";
    }
}
