package pojo;

public class CandidateAssociatedEntFields {

	private String dataType;

    private String name;

    private String type;

    public String getDataType ()
    {
        return dataType;
    }

    public void setDataType (String dataType)
    {
        this.dataType = dataType;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    @Override
    public String toString()
    {
        return "Associated entity options: [dataType = "+dataType+", name = "+name+", type = "+type+"]";
    }
}
