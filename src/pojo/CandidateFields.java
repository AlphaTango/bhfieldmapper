package pojo;

import java.util.ArrayList;

public class CandidateFields {
	
    private String optionsUrl;

    private String sortOrder;

    private String hint;

    private String readOnly;

    private String optionsType;

    private String description;

    private String optional;

    private String inputType;

    private String multiValue;

    private String required;

	private String dataType;

	private String hideFromSearch;

	private String maxLength;

	private String name;

	private String label;

	private String type;

	private String confidential;
	
	private String dataSpecialization;
	
	private CandidateFields[] fields;

	private CandidateOptions[] options;

	public String getOptionsUrl() {
		return optionsUrl;
	}

	public void setOptionsUrl(String optionsUrl) {
		this.optionsUrl = optionsUrl;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getReadOnly() {
		return readOnly;
	}

	public void setReadOnly(String readOnly) {
		this.readOnly = readOnly;
	}

	public String getOptionsType() {
		return optionsType;
	}

	public void setOptionsType(String optionsType) {
		this.optionsType = optionsType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getOptional() {
		return optional;
	}

	public void setOptional(String optional) {
		this.optional = optional;
	}

	public String getInputType() {
		return inputType;
	}

	public void setInputType(String inputType) {
		this.inputType = inputType;
	}

	public String getMultiValue() {
		return multiValue;
	}

	public void setMultiValue(String multiValue) {
		this.multiValue = multiValue;
	}

	public String getRequired() {
		return required;
	}

	public void setRequired(String required) {
		this.required = required;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getHideFromSearch() {
		return hideFromSearch;
	}

	public void setHideFromSearch(String hideFromSearch) {
		this.hideFromSearch = hideFromSearch;
	}

	public String getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(String maxLength) {
		this.maxLength = maxLength;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getConfidential() {
		return confidential;
	}

	public void setConfidential(String confidential) {
		this.confidential = confidential;
	}
	
    public String getDataSpecialization() {
		return dataSpecialization;
	}

	public void setDataSpecialization(String dataSpecialization) {
		this.dataSpecialization = dataSpecialization;
	}

	public CandidateFields[] getFields ()
    {
        return fields;
    }

    public void setFields (CandidateFields[] fields)
    {
        this.fields = fields;
    }

	public CandidateOptions[] getOptions() {
		return options;
	}
	
	public String optionsToCsv() {
		String csv = null;
		
		int numOptions = getOptions().length;
		csv = "\"";
		for (int j = 0; j < numOptions - 1; j++) {		// loop through all the options except the last
			csv += getOptions()[j].csvOutput().concat(",");	// add every option into the last element of the current (row) array
		}
		
		csv += getOptions()[numOptions - 1].csvOutput().concat("\"");
		
		return csv;
	}

	public void setOptions(CandidateOptions[] options) {
		this.options = options;
	}

	@Override
	public String toString() {
		return "Field: [dataType = " + dataType + ", hideFromSearch = "
				+ hideFromSearch + ", maxLength = " + maxLength + ", name = "
				+ name + ", label = " + label + ", type = " + type
				+ ", confidential = " + confidential + ", options = " + options
				+ "]";
	}

	// don't use - doesn't include dataSpecialization !!
	public String[] csvArray(){
		String[] result = new String[18];
		result[0] = name;
		result[1] = label;
		result[2] = dataType;
		result[3] = type;
		result[4] = optionsType;
		result[5] = inputType;
		result[6] = multiValue;
		result[7] = readOnly;
		result[8] = required;
		result[9] = sortOrder;
		result[10] = maxLength;
		result[11] = hideFromSearch;
		result[12] = confidential;
		result[13] = optional;
		result[14] = hint;
		result[15] = description;
		result[16] = "";			// options will be filled in here
		result[17] = optionsUrl;
		
		return result;
	}
	
	public ArrayList<String> csvList() {
		ArrayList<String> result = new ArrayList<String>();
		result.add(name);
		result.add(label);
		result.add(dataType);
		result.add(type);
		result.add(optionsType);
		result.add(inputType);
		result.add(multiValue);
		result.add(readOnly);
		result.add(required);
		result.add(sortOrder);
		result.add(maxLength);
		result.add(hideFromSearch);
		result.add(confidential);
		result.add(optional);
		result.add(hint);
		result.add(description);
		result.add(dataSpecialization);
		result.add("");
		result.add(optionsUrl);

		return result;
	}
	
	public String csvOutput() {
		return name	+ "," + label + "," + dataType + "," + type + "," + optionsType + "," + inputType + "," + multiValue + "," + readOnly + "," + required + "," + sortOrder + "," + 
				maxLength + "," + hideFromSearch + "," + confidential  + "," + optional + "," + hint + "," + description + "," + dataSpecialization /*optionsUrl + "," +*/ 
				/* + "," + * options + "]" */;
	}


	


}
