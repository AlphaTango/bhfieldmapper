package pojo;

public class CandidateOptions {

    private String value;

    private String label;

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    public String getLabel ()
    {
        return label;
    }

    public void setLabel (String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return "Options: [value = "+value+", label = "+label+"]";
    }
    
    public String csvOutput()
    {
        return value /*+ "," + label*/;
    }
	
}
