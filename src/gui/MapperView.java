package gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import org.alphatango.console.Console;

import guiComponents.ControlPanel;
import guiComponents.CredsPanelV2;
import guiComponents.EntitiesPanel;
import guiComponents.RestTokenPanel;
import guiComponents.SelectionPanel;
import mapper.MapperController;

public class MapperView extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4135096727520081962L;
	
	private JTabbedPane tabbedPane;
	private CredsPanelV2 credsPanel;
	private RestTokenPanel tokenPanel;
	
	private ControlPanel controlPanel;
	private SelectionPanel selectionPanel;
	private EntitiesPanel entitiesPanel;
	
	private JPanel consolePanel;
	private Console console;
	
	public MapperView() {
		
		console = Console.getInstance();
		console.setOutputLevel(2);
		
		getContentPane().setSize(new Dimension(640, 382));
		getContentPane().setMaximumSize(new Dimension(640, 382));
		setTitle("Bullhorn Field Mapper Exporter ver: " + MapperController.ver);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setMinimumSize(new Dimension(650, 298));
		setSize(650, 650);
		getContentPane().setMinimumSize(new Dimension(640, 382));
		getContentPane().setLayout(new GridLayout(0, 1, 0, 0));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(null);	// TODO: update this to use gridbaglayout
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 634, 110);
		panel.add(tabbedPane);
		
		// create + add CredsPanel
		credsPanel = new CredsPanelV2();
		tabbedPane.addTab("Username", null, credsPanel, null);
		
		// hide unused components
		//credsPanel.getConsoleLevelComboBox().setVisible(false);
		
		//credsPanel.getLblConsoleLvlCmbx().setVisible(false);
		credsPanel.getCbAlwaysOnTop().setVisible(false);
		credsPanel.getBtnConnect().setVisible(false);
		
		// create + add REST token creds panel
		tokenPanel = new RestTokenPanel();
		tabbedPane.addTab("REST Token", null, tokenPanel, null);

		// create + add control panel (show/hide console, verbose mode, include system fields, plid list, download btn)
		controlPanel = new ControlPanel();
		controlPanel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		controlPanel.setBounds(0, 108, 634, 33);
		panel.add(controlPanel);
		
		// create + add entity selection panel
		selectionPanel = new SelectionPanel();
		selectionPanel.setBounds(0, 152, 634, 33);
		panel.add(selectionPanel);
		
		// create + add entities checkbox panel
		entitiesPanel = new EntitiesPanel();
		entitiesPanel.setBounds(0, 196, 634, 54);
		panel.add(entitiesPanel);
		entitiesPanel.setLayout(new GridLayout(4, 5));
	
		// create + add console panel
		consolePanel = new JPanel();
		consolePanel.setBounds(0, 255, 634, 356);
		consolePanel.setLayout(new BorderLayout());
		consolePanel.add(console.getScrollPane(), BorderLayout.CENTER);
		//consolePanel.setPreferredSize(new Dimension(consolePanel.getPreferredSize().width, 175));

		panel.add(consolePanel/*, e*/);

	}
	
	public void addActionListeners(ActionListener actionListener){	// seems to degrade performance when app is loading, each call blocks?
		controlPanel.getBtnDownload().addActionListener(actionListener);				// create a separate method for each button instead?
		selectionPanel.getBtnDefault().addActionListener(actionListener);
		selectionPanel.getBtnJobTracks().addActionListener(actionListener);
		selectionPanel.getBtnPlacementTracks().addActionListener(actionListener);
		selectionPanel.getBtnEnterpriseCrm().addActionListener(actionListener);
		selectionPanel.getBtnSelectAll().addActionListener(actionListener);
		selectionPanel.getBtnDeselectAll().addActionListener(actionListener);
		
		//controlPanel.getConsoleLevelComboBox().addActionListener(actionListener);
		
		credsPanel.getConsoleLevelComboBox().addActionListener(actionListener);
		//controlPanel.getCbShowConsole().addActionListener(actionListener);
		//controlPanel.getCbVerboseOutput().addActionListener(actionListener);
		controlPanel.getCbExcludeSystem().addActionListener(actionListener);
		credsPanel.getBtnConnect().addActionListener(actionListener);
	}
	
	public void addCloseListener(WindowListener closeListener){	
		this.addWindowListener(closeListener);
	}

	
	public String[] getCreds() {
		return credsPanel.getCreds();
	}

	/**
	 * @return the tfUsername
	 */
	public JTextField getTfUsername() {
		return credsPanel.getTfUsername();
	}

	/**
	 * @param tfUsername the tfUsername to set
	 */
	public void setTfUsername(JTextField tfUsername) {
		credsPanel.setTfUsername(tfUsername);
	}

	/**
	 * @return the tfClientId
	 */
	public JTextField getTfClientId() {
		return credsPanel.getTfClientId();
	}

	/**
	 * @param tfClientId the tfClientId to set
	 */
	public void setTfClientId(JTextField tfClientId) {
		credsPanel.setTfClientId(tfClientId);
	}

	/**
	 * @return the tfPassword
	 */
	public JTextField getTfPassword() {
		return credsPanel.getTfPassword();
	}

	/**
	 * @param tfPassword the tfPassword to set
	 */
	public void setTfPassword(JTextField tfPassword) {
		credsPanel.setTfPassword(tfPassword);
	}

	/**
	 * @return the tfClientSecret
	 */
	public JTextField getTfClientSecret() {
		return credsPanel.getTfClientSecret();
	}

	/**
	 * @param tfClientSecret the tfClientSecret to set
	 */
	public void setTfClientSecret(JTextField tfClientSecret) {
		credsPanel.setTfClientSecret(tfClientSecret);
	}

	/**
	 * @return the tfSl
	 */
	public JTextField getTfSl() {
		return tokenPanel.getTfSl();
	}


	/**
	 * @param tfSl the tfSl to set
	 */
	public void setTfSl(JTextField tfSl) {
		tokenPanel.setTfSl(tfSl);
	}


	/**
	 * @return the tfCorpToken
	 */
	public JTextField getTfCorpToken() {
		return tokenPanel.getTfCorpToken();
	}


	/**
	 * @param tfCorpToken the tfCorpToken to set
	 */
	public void setTfCorpToken(JTextField tfCorpToken) {
		tokenPanel.setTfCorpToken(tfCorpToken);
	}


	/**
	 * @return the tfRestToken
	 */
	public JTextField getTfRestToken() {
		return tokenPanel.getTfRestToken();
	}


	/**
	 * @param tfRestToken the tfRestToken to set
	 */
	public void setTfRestToken(JTextField tfRestToken) {
		tokenPanel.setTfRestToken(tfRestToken);
	}
	
	
	//credsPanel.getConsoleLevelComboBox();
	/**
	 * @return the ConsoleLevelComboBox
	 */
	public JComboBox<String> getConsoleLevelComboBox() {
		return credsPanel.getConsoleLevelComboBox();
	}

	/**
	//  @param ConsoleLevelComboBox the ConsoleLevelComboBox to set
	**/
	public void setConsoleLevelComboBox(JComboBox<String> consoleLevelComboBox) {
		credsPanel.setConsoleLevelComboBox(consoleLevelComboBox);
	}

	/**
	 * @return the chckbxShowConsole
	 */
/*	public JCheckBox getCbShowConsole() {
		return controlPanel.getCbShowConsole();
	}

	*//**
	 * @param chckbxShowConsole the chckbxShowConsole to set
	 *//*
	public void setCbShowConsole(JCheckBox chckbxShowConsole) {
		controlPanel.setCbShowConsole(chckbxShowConsole);
	}*/
	
	public JCheckBox getCbExcludeSystem() {
		return controlPanel.getCbExcludeSystem();
	}

	public void setCbExcludeSystem(JCheckBox cbExcludeSystem) {
		controlPanel.setCbExcludeSystem(cbExcludeSystem);
	}

	/**
	 * @return the btnDownload
	 */
	public JButton getBtnDownload() {
		return controlPanel.getBtnDownload();
	}


	/**
	 * @param btnDownload the btnDownload to set
	 */
	public void setBtnDownload(JButton btnDownload) {
		controlPanel.setBtnDownload(btnDownload);
	}

	/**
	 * @return the chkBoxes
	 */
	public JCheckBox[] getChkBoxes() {
		return entitiesPanel.getChkBoxes();
	}

	/**
	 * @param chkBoxes the chkBoxes to set
	 */
	public void setChkBoxes(JCheckBox[] chkBoxes) {
		entitiesPanel.setChkBoxes(chkBoxes);
	}

	/**
	 * @return the tfPlid
	 */
	public JTextField getTfPlid() {
		return controlPanel.getTfPlid();
	}

	/**
	 * @param tfPlid the tfPlid to set
	 */
	public void setTfPlid(JTextField tfPlid) {
		controlPanel.setTfPlid(tfPlid);
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public void setTabbedPane(JTabbedPane tabbedPane) {
		this.tabbedPane = tabbedPane;
	}
}