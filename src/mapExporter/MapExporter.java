package mapExporter;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.swing.JCheckBox;

import org.alphatango.console.Console;
import org.alphatango.console.Console.ConsoleLevel;

import com.google.gson.Gson;

import BhTokenGenerator.BhTokenGenerator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
//import consoleMVC.ConsoleSingleton;
import pojo.CandidateAPI;

/**
 * 
 * @author Leigh Plumley <br><br>
 *
 *	TODO:	- currently exporting default value list (options columns), what about display value list/value list? 
 */
public class MapExporter implements Runnable {

	OkHttpClient client = new OkHttpClient(); // create new OkHttpClient - TODO: switch to using the existing one in BhTokenGen
	Console console = Console.getInstance();
	
	BhTokenGenerator bhTokenGenerator;	// passed in via cstor
	File fileDir;	// passed in via cstor
	
	JCheckBox[] entities;
	String[] plid = null;
	
	volatile int rows;
	volatile int i;
	private String callerName = "MapExporter"; 	// for console messages
	
	private Boolean includeSystem = true; // don't include system fields in export - TODO: pass in from UI
	
	/**
	 * 
	 */
	public MapExporter(BhTokenGenerator bhTokenGenerator, File fileDir) {
		this.bhTokenGenerator = bhTokenGenerator;
		this.fileDir = fileDir;
		console.out("System fields included: " + includeSystem.toString(), ConsoleLevel.INFO, callerName);
	}
	
	private void writeCSV(String url, File file){ 	//mode: 0 = soap, 1 = rest
		
		String[][] output = parseJSON(url);	// [rows][columns = 18]
		
		if (output == null){
			console.out("No input, file write abandonned", 2, callerName);
			return;
		}
		
		console.out("Writing file: " + file.getAbsolutePath(), ConsoleLevel.INFO, callerName);
		PrintWriter custOutput = createFile(file);
		
		custOutput.println("name,label,dataType,type,optionsType,inputType,multiValue,readOnly,required,sortOrder,maxLength,hideFromSearch,confidential,optional,hint,description,dataSpecialization,options,optionsURL");
		
		for(String[] a : output){
			for(int g = 0; g <= 18; g++){ 
				if (g == 18){
				custOutput.print(a[g]);
				}
				else{
					custOutput.print(a[g] + ",");
				}
			}
			custOutput.println();
		}
		custOutput.flush();
		custOutput.close();	
		console.out("File write complete", 0, callerName);
	}
	
	
	/**
	 * 
	 * @param file
	 * @return
	 */
	private static PrintWriter createFile(File file){
		
		File listOfNames = file;	// TODO: update
		PrintWriter infoToWrite;
		try {
			infoToWrite = new PrintWriter(new BufferedWriter(new FileWriter(listOfNames)));
			return infoToWrite;
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("An I/O Error Occurred writing the file");	// TODO: update
		}
		return null;
	}
	
	
	/**
	 * Get JSON data from the provided URL
	 * @param url - the URL for the JSON data 
	 * @return JSON data as a String
	 */
	private String getJSON(String url) {
		
		Request request = new Request.Builder().url(url).build();
		try {
			Response response = client.newCall(request).execute();
			return response.body().string();
		} catch (IOException e) {
			e.printStackTrace();
			return "Error getting data";
		}
	}
	
	/** parse received data
	 * 
	 * @param url - the URL for the JSON data
	 * @return String[rows][columns] - JSON data as a 2D array of rows and columns
	 */

	public String[][] parseJSON(String url) {

		console.out("connecting to: " + url, 0, callerName);
		String json = null;
		String[][] fullArray = null;
		json = getJSON(url);	// get JSON as a String from the specified URL
		
		console.out(json, 3, callerName );

		Gson gson = new Gson();
		CandidateAPI capi = gson.fromJson(json, CandidateAPI.class);	// convert the JSON String to POJOs 

		rows = 0;
		try{
			rows = capi.getFields().length; // check if POJO has data
		}
		catch (NullPointerException e ){	// no data
			console.out("Error in parseJSON(): Entity does not exist", 2, callerName);
			return null;
		}

		ArrayList<ArrayList<String>> resultList = new ArrayList<ArrayList<String>>(); // arraylist to hold a row

		int count = 0; // address fields
		i = 0;
		int currentRow = 0;
		while (i < rows) {	// for each row in the main input (which has only 1 field for address/secondaryAddress)
			
			// check the fieldType
			String fieldType = "";
			fieldType = capi.getFields()[i].getDataType();
			if (fieldType == null) fieldType = "";
			
			System.out.println(fieldType);
			// if the current row is a SYSTEM field
			if  ( (capi.getFields()[i].getDataSpecialization() != null // check for null or a nullpointerexception might be thrown
					&& capi.getFields()[i].getDataSpecialization().equals("SYSTEM") ) // check whether this is a SYSTEM field 
					&& ( !fieldType.contains("Address"))  ) { // AND the row is not an Address field 
				System.out.println("non-address SYSTEM field detected: " + capi.getFields()[i].csvOutput()); // debug
				if ( ! includeSystem) { //if include system field is NOT enabled
					i++;				// skip this field
					continue;
				}
			}

			resultList.add(capi.getFields()[i].csvList()); // get the fields in the current row as an arraylist of strings and add them as a new row
			
			//need to do this again inside the get address fields loop below. extract into own method?
			if (capi.getFields()[i].getOptions() == null) {	// if there are no options for this field
				resultList.get(currentRow).set(17, "");		// set options to ""
			}

			else if(capi.getFields()[i].getOptions() != null){	// otherwise if there are options, add them to the list
				resultList.get(currentRow).set(17, capi.getFields()[i].optionsToCsv());	
			}


			if (capi.getFields()[i].csvList().get(2) != null && capi.getFields()[i].csvList().get(2).contains("Address")) {		// If this is an Address field
				if (count == 0) {
					for (int n = 0; n < capi.getFields()[i].getFields().length; n++ ) {	// for each sub-field in the address field
						resultList.add(capi.getFields()[i].getFields()[n].csvList());	// add the current address sub-field as a row
						currentRow++;
					}
				}
				else if (count == 1){
					for (int n = 0; n < capi.getFields()[i].getFields().length; n++ ) {	// for each sub-field in the address field
						resultList.add(capi.getFields()[i].getFields()[n].csvList());	// add the current address sub-field as a row
						currentRow++;
					}
				}
				count++;		// only expecting 2 address fields - not ideal!
			}
			currentRow++;
			i++;
		}



		fullArray = new String[resultList.size()][];

		// Convert the 2D Arraylist to a 2D array
		for (int j = 0; j < resultList.size(); j ++) {

			ArrayList<String> row = resultList.get(j);
			String[] copy = new String[row.size()];

			for (int k = 0; k < row.size() ; k++){
				copy[k] = row.get(k);
			}
			fullArray[j] = copy;
		}
		return fullArray;
	}
	
	
	@Override
	public void run() {
		//if authman not connected, connect
		
		if (!bhTokenGenerator.isConnected()){
			Thread auth = new Thread(bhTokenGenerator);
			auth.start();
			console.out("Connecting", ConsoleLevel.INFO, "AuthMan");
			
			while (!bhTokenGenerator.isConnected()){ /* wait*/ 
				try {
					auth.join();
					//Thread.sleep(50);
					//auth.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		if (bhTokenGenerator.isConnected()) {
			if(plid == null){
				getMap("");
			}
			else {
				for (String j : plid){
					getMap(j);
				}
			}
			console.out("Download(s) complete", 0, callerName);
		}
		else console.out("Not connected", 1, callerName);
	}
	
	private void getMap(String plidString){
		for (JCheckBox k : entities) {	// use controller instead
			if (k.isSelected()) {
				String name = k.getName();
				File fileName = null;
				String url = null;
				url = bhTokenGenerator.getRestUrl() + "/meta/" + name + "?fields=*&BhRestToken=";
				
				if (plidString.equals("")) {
					console.out("Getting field map: " + name + " for default PLID", 0, callerName);
					url += bhTokenGenerator.getBhToken() + "&meta=full";
					fileName = new File(fileDir.getPath() + "\\" + name + ".csv");
				}
				else { 
					console.out("Getting field map: " + name + " for PLID: " + plidString, 0, callerName);
					url += bhTokenGenerator.getBhToken() + "&meta=full" + "&privateLabelId=" + plidString;
					fileName = new File(fileDir.getPath() + "\\" + plidString + " - " + name + ".csv"); 
				}

				writeCSV(url, fileName);
			}
		}
	}

	/**
	 * @param entities the entities to set
	 */
	public void setEntities(JCheckBox[] entities) {
		this.entities = entities;
	}

	/**
	 * @param plid the plid to set
	 */
	public void setPlid(String[] plid) {
		this.plid = plid;
	}

	public Boolean getIncludeSystem() {
		return includeSystem;
	}

	public void setIncludeSystem(Boolean includeSystem) {
		this.includeSystem = includeSystem;
	}

}
