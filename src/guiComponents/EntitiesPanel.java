package guiComponents;

import javax.swing.JCheckBox;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class EntitiesPanel extends JPanel {
	
	private JCheckBox Candidate = new JCheckBox("Candidate");
	private JCheckBox ClientContact = new JCheckBox("ClientContact");
	private JCheckBox ClientCorporation = new JCheckBox("Company");
	private JCheckBox JobOrder = new JCheckBox("Job");
	private JCheckBox JobOrder2 = new JCheckBox("Job II");
	private JCheckBox JobOrder3 = new JCheckBox("Job III");
	private JCheckBox JobOrder4 = new JCheckBox("Job IV");
	private JCheckBox JobOrder5 = new JCheckBox("Job V");
	private JCheckBox Lead = new JCheckBox("Lead");
	private JCheckBox Opportunity = new JCheckBox("Opportunity");
	private JCheckBox Opportunity2 = new JCheckBox("Opportunity II");
	private JCheckBox Opportunity3 = new JCheckBox("Opportunity III");
	private JCheckBox Opportunity4 = new JCheckBox("Opportunity IV");
	private JCheckBox Opportunity5 = new JCheckBox("Opportunity V");
	private JCheckBox Placement = new JCheckBox("Placement");
	private JCheckBox Placement2 = new JCheckBox("Placement II");
	private JCheckBox Placement3 = new JCheckBox("Placement III");
	private JCheckBox Placement4 = new JCheckBox("Placement IV");
	private JCheckBox Placement5 = new JCheckBox("Placement V");
	
	private JCheckBox[] chkBoxes = {Candidate, ClientContact, ClientCorporation, JobOrder, JobOrder2, JobOrder3, JobOrder4, JobOrder5,
		Lead, Opportunity, Opportunity2, Opportunity3, Opportunity4, Opportunity5, Placement, Placement2, Placement3, Placement4, Placement5};

	public EntitiesPanel() {

		super();
		
		Candidate.setSelected(true);
		Candidate.setName("Candidate");
		
		ClientContact.setSelected(true);
		ClientContact.setName("ClientContact");
		
		ClientCorporation.setSelected(true);
		ClientCorporation.setName("ClientCorporation");
		
		JobOrder.setSelected(true);
		JobOrder.setName("JobOrder");
		
		JobOrder2.setSelected(true);
		JobOrder2.setName("JobOrder2");
		
		//row 2
		JobOrder3.setSelected(true);
		JobOrder3.setName("JobOrder3");
		
		JobOrder4.setSelected(true);
		JobOrder4.setName("JobOrder4");
		
		JobOrder5.setSelected(true);
		JobOrder5.setName("JobOrder5");
		
		//Lead.setSelected(true);
		Lead.setName("Lead");

		//Opportunity.setSelected(true);
		Opportunity.setName("Opportunity");
		Opportunity2.setName("Opportunity2");
		Opportunity3.setName("Opportunity3");
		Opportunity4.setName("Opportunity4");
		Opportunity5.setName("Opportunity5");
				
		//row 3
		Placement.setSelected(true);
		Placement.setName("Placement");
		Placement2.setName("Placement2");
		Placement3.setName("Placement3");
		Placement4.setName("Placement4");
		Placement5.setName("Placement5");
		
		for (JCheckBox j : chkBoxes){
			add(j);
			
		}
		
		
	}

	public JCheckBox[] getChkBoxes() {
		return chkBoxes;
	}

	public void setChkBoxes(JCheckBox[] chkBoxes) {
		this.chkBoxes = chkBoxes;
	}
	
	


}
