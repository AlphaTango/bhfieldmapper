package guiComponents;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

@SuppressWarnings("serial")
public class ControlPanel extends JPanel {

	//private JCheckBox cbShowConsole;
	//private JCheckBox cbVerboseOutput;
	private JCheckBox cbExcludeSystem;
	private JButton btnDownload;
	private JLabel lblPrivateLabelIds;
	private JTextField tfPlid;
	private JLabel lblConsoleLvlCmbx;
	private JComboBox<String> consoleLevelComboBox;

	public ControlPanel() {
		
		super();
		//setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		
		lblConsoleLvlCmbx = new JLabel("Console: ");
		lblConsoleLvlCmbx.setHorizontalAlignment(SwingConstants.RIGHT);
		add(lblConsoleLvlCmbx);
		
		consoleLevelComboBox = new JComboBox<String>();
		add(consoleLevelComboBox);
		consoleLevelComboBox.addItem("Off"); 		
		consoleLevelComboBox.addItem("Info"); 		
		consoleLevelComboBox.addItem("Warning");	
		consoleLevelComboBox.addItem("Error");		
		consoleLevelComboBox.setSelectedIndex(1);	// show console on startup
		
		/*
		// console show/hide checkbox
		cbShowConsole = new JCheckBox("Console");
		//cbShowConsole.setActionCommand("Show console");	// mapper 
		cbShowConsole.setSelected(true);
		cbShowConsole.setToolTipText("Show/hide console panel");
		cbShowConsole.setBounds(6, 5, 80, 23);
		add(cbShowConsole);*/
		
		// console verbose mode checkbox
		/*cbVerboseOutput = new JCheckBox("Verbose");
		//cbVerboseOutput.setActionCommand("Verbose output");
		cbVerboseOutput.setToolTipText("Full console output");
		cbVerboseOutput.setBounds(88, 5, 80, 23);
		add(cbVerboseOutput);*/
		
		// system fields include/exclude checkbox
		cbExcludeSystem = new JCheckBox("System Fields");
		cbExcludeSystem.setSelected(true);
		cbExcludeSystem.setToolTipText("Include/exclude fields with SYSTEM dataSpecialization (does not apply to 'Address' fields, which are always included)");
		cbExcludeSystem.setBounds(170, 5, 120, 23);
		add(cbExcludeSystem);
		
		// private label ID
		lblPrivateLabelIds = new JLabel("Private Label IDs:");
		lblPrivateLabelIds.setToolTipText("Optional - one or more Private Label IDs, comma separated");
		lblPrivateLabelIds.setHorizontalAlignment(SwingConstants.RIGHT);
		lblPrivateLabelIds.setBounds(258, 9, 134, 14);
		add(lblPrivateLabelIds);
		
		tfPlid = new JTextField();
		tfPlid.setToolTipText("Optional - one or more Private Label IDs, comma separated");
		tfPlid.setBounds(405, 6, 108, 20);
		add(tfPlid);
		tfPlid.setColumns(10);
		
		// download button
		btnDownload = new JButton("Download");
		btnDownload.setBounds(523, 5, 101, 23);
		add(btnDownload);
		
	}

	/*public JCheckBox getCbShowConsole() {
		return cbShowConsole;
	}

	public void setCbShowConsole(JCheckBox cbShowConsole) {
		this.cbShowConsole = cbShowConsole;
	}*/

	public JComboBox<String> getConsoleLevelComboBox() {
		return consoleLevelComboBox;
	}

	public void setConsoleLevelComboBox(JComboBox<String> consoleLevelComboBox) {
		this.consoleLevelComboBox = consoleLevelComboBox;
	}

	/*public JCheckBox getCbVerboseOutput() {
		return cbVerboseOutput;
	}

	public void setCbVerboseOutput(JCheckBox cbVerboseOutput) {
		this.cbVerboseOutput = cbVerboseOutput;
	}*/

	public JCheckBox getCbExcludeSystem() {
		return cbExcludeSystem;
	}

	public void setCbExcludeSystem(JCheckBox cbExcludeSystem) {
		this.cbExcludeSystem = cbExcludeSystem;
	}

	public JButton getBtnDownload() {
		return btnDownload;
	}

	public void setBtnDownload(JButton btnDownload) {
		this.btnDownload = btnDownload;
	}

	public JLabel getLblPrivateLabelIds() {
		return lblPrivateLabelIds;
	}

	public void setLblPrivateLabelIds(JLabel lblPrivateLabelIds) {
		this.lblPrivateLabelIds = lblPrivateLabelIds;
	}

	public JTextField getTfPlid() {
		return tfPlid;
	}

	public void setTfPlid(JTextField tfPlid) {
		this.tfPlid = tfPlid;
	}
	
}
