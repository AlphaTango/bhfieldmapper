package guiComponents;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import java.awt.FlowLayout;

@SuppressWarnings("serial")
public class RestTokenPanel extends JPanel {

	private JLabel lblSl;
	private JPanel tokenPanel;
	private JTextField tfSl;
	private JLabel lblCorpToken;
	private JTextField tfCorpToken;
	private JLabel lblRestToken;
	private JTextField tfRestToken;

	public RestTokenPanel() {
		
		super();
	
		this.setBounds(0, 0, 624, 85);
		
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		// swimlane
		lblSl = new JLabel("Swimlane:");
		lblSl.setHorizontalAlignment(SwingConstants.RIGHT);
		add(lblSl);
		
		tfSl = new JTextField();
		add(tfSl);
		tfSl.setColumns(2);
		
		// corp token
		lblCorpToken = new JLabel("Corp Token:");
		lblCorpToken.setHorizontalAlignment(SwingConstants.RIGHT);
		add(lblCorpToken);
		
		tfCorpToken = new JTextField();
		tfCorpToken.setToolTipText("<html>Corp <b>TOKEN</b>, not <b>ID</b></html>");
		add(tfCorpToken);
		tfCorpToken.setColumns(7);
		
		// rest token
		lblRestToken = DefaultComponentFactory.getInstance().createLabel("REST Token:");
		lblRestToken.setHorizontalAlignment(SwingConstants.RIGHT);
		add(lblRestToken);
		
		tfRestToken = new JTextField();
		add(tfRestToken);
		tfRestToken.setColumns(20);
	}

	// getters and setters TODO remove unnecessary setters?
	public JLabel getLblSl() {
		return lblSl;
	}

	public void setLblSl(JLabel lblSl) {
		this.lblSl = lblSl;
	}

	public JPanel getTokenPanel() {
		return tokenPanel;
	}

	public void setTokenPanel(JPanel tokenPanel) {
		this.tokenPanel = tokenPanel;
	}

	public JTextField getTfSl() {
		return tfSl;
	}

	public void setTfSl(JTextField tfSl) {
		this.tfSl = tfSl;
	}

	public JLabel getLblCorpToken() {
		return lblCorpToken;
	}

	public void setLblCorpToken(JLabel lblCorpToken) {
		this.lblCorpToken = lblCorpToken;
	}

	public JTextField getTfCorpToken() {
		return tfCorpToken;
	}

	public void setTfCorpToken(JTextField tfCorpToken) {
		this.tfCorpToken = tfCorpToken;
	}

	public JLabel getLblRestToken() {
		return lblRestToken;
	}

	public void setLblRestToken(JLabel lblRestToken) {
		this.lblRestToken = lblRestToken;
	}

	public JTextField getTfRestToken() {
		return tfRestToken;
	}

	public void setTfRestToken(JTextField tfRestToken) {
		this.tfRestToken = tfRestToken;
	}
	
}
