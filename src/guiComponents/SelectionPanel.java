package guiComponents;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class SelectionPanel extends JPanel {
	
	private JButton btnDefault;
	private JButton btnEnterpriseCrm;
	private JButton btnJobTracks;
	private JButton btnPlacementTracks;
	private JButton btnSelectAll;
	private JButton btnDeselectAll;

	public SelectionPanel() {
		
		super();
		
		btnDefault = new JButton("Default");
		btnDefault.setName("");
		btnDefault.setToolTipText("Candidate, Contact, Company, Submissions, Job, Placement");
		add(btnDefault);
		
		btnEnterpriseCrm = new JButton("Enterprise+CRM");
		btnEnterpriseCrm.setToolTipText("Default + Leads and Opps (no tracks)");
		add(btnEnterpriseCrm);
		
		btnJobTracks = new JButton("+Job Tracks");
		btnJobTracks.setToolTipText("Job tracks I - V");
		add(btnJobTracks);
		
		btnPlacementTracks = new JButton("+Placement Tracks");
		btnPlacementTracks.setToolTipText("Placement tracks I - V");
		add(btnPlacementTracks);
		
		btnSelectAll = new JButton("All");
		btnSelectAll.setActionCommand("Select all");
		add(btnSelectAll);
		
		btnDeselectAll = new JButton("None");
		btnDeselectAll.setActionCommand("Deselect all");
		add(btnDeselectAll);
	}

	public JButton getBtnDefault() {
		return btnDefault;
	}

	public void setBtnDefault(JButton btnDefault) {
		this.btnDefault = btnDefault;
	}

	public JButton getBtnEnterpriseCrm() {
		return btnEnterpriseCrm;
	}

	public void setBtnEnterpriseCrm(JButton btnEnterpriseCrm) {
		this.btnEnterpriseCrm = btnEnterpriseCrm;
	}

	public JButton getBtnJobTracks() {
		return btnJobTracks;
	}

	public void setBtnJobTracks(JButton btnJobTracks) {
		this.btnJobTracks = btnJobTracks;
	}

	public JButton getBtnPlacementTracks() {
		return btnPlacementTracks;
	}

	public void setBtnPlacementTracks(JButton btnPlacementTracks) {
		this.btnPlacementTracks = btnPlacementTracks;
	}

	public JButton getBtnSelectAll() {
		return btnSelectAll;
	}

	public void setBtnSelectAll(JButton btnSelectAll) {
		this.btnSelectAll = btnSelectAll;
	}

	public JButton getBtnDeselectAll() {
		return btnDeselectAll;
	}

	public void setBtnDeselectAll(JButton btnDeselectAll) {
		this.btnDeselectAll = btnDeselectAll;
	}
	
}
