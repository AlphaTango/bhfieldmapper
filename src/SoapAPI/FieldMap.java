package SoapAPI;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "root")
public class FieldMap
{
	private FieldMaps[] fieldMaps;

    public FieldMaps[] getFieldMaps ()
    {
        return fieldMaps;
    }

    public void setFieldMaps (FieldMaps[] fieldMaps)
    {
        this.fieldMaps = fieldMaps;
    }

    @Override
    public String toString()
    {
        return fieldMaps.toString();
    }
}