package SoapAPI;

public class SessReturn {

	    private String corporationId;

	    private String session;

	    private String userId;

	    public String getCorporationId ()
	    {
	        return corporationId;
	    }

	    public void setCorporationId (String corporationId)
	    {
	        this.corporationId = corporationId;
	    }

	    public String getSession ()
	    {
	        return session;
	    }

	    public void setSession (String session)
	    {
	        this.session = session;
	    }

	    public String getUserId ()
	    {
	        return userId;
	    }

	    public void setUserId (String userId)
	    {
	        this.userId = userId;
	    }

	    @Override
	    public String toString()
	    {
	        return "corporationId = " + corporationId + ", session = " + session + ", userId = " + userId;
	    }
	
}
