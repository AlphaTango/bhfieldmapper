package SoapAPI;

public class SessResponse {
	
	
	    private SessReturn sessReturn;

	    public SessReturn getSessReturn ()
	    {
	        return sessReturn;
	    }

	    public void setSessReturn (SessReturn sessReturn)
	    {
	        this.sessReturn = sessReturn;
	    }

		@Override
	    public String toString()
	    {
	        return sessReturn.toString();
	    }
	}
