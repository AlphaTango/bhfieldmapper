# README #

## Field Map Exporter for Bullhorn

**The most recent version of my Field Map extraction tool for Bullhorn ATS/CRM is now available:**  
Direct download link:  [here](https://bitbucket.org/AlphaTango/bhfieldmapper/raw/fb245c00ee158f31042cc4be41308a9a4d147ac6/BhFieldMapper.jar) - Chrome may warn you about downloading JAR files, just click 'Keep'. Steps for building form source will be added shortly.

![Bullhorn Field Map Exporter](http://i.imgur.com/GupZQ2H.png)
  
**- What does this do?**  
This tool allows you to extract your Bullhorn field mappings to a CSV file. This is useful for creating a backup of your current field mappings before making changes and prior to migrating data into/out of the system.  

**- What does this not do?**  
You can't import field maps from CSV at this time (I'm looking into the possibility of this for a future release). You also can't currently extract the maps for Opportunity tracks II-V, Note, Email Template or any of the other non-editable/secondary entities (again this may change in future, if you'd like to see this option, [let me know](contact.html)).  

**- What do I need to run this?**  
The latest [Java Runtime Environment](https://www.java.com/en/download/)  
Access to the Bullhorn REST API (Client ID, Client Secret and a developer account)  

**- Who is this for?**  
Bullhorn Superusers, System Integrators and Technical Consultants.  

**- What else do I need to know?**  
The address fields are exported as they appear in the User Interface in Bullhorn, but note that technically they're stored slightly differently in the system (as a Composite field, you can think of this as a sub-field or an inner JSON array; there's more info on this in the REST API documentation at [http://developer.bullhorn.com](http://developer.bullhorn.com) ).  
If you don't specify a Private Label ID it defaults to the PLID with the lowest ID number.  
It will overwrite existing CSV files with the same name without warning so be careful where you save them to.  
This software is provided under the Creative Commons BY-SA license, full details [here]( https://creativecommons.org/licenses/by-sa/4.0/legalcode).